<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Enerkom</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="assets/vendor/remixicon/remixicon.css" rel="stylesheet">
  <link href="assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
  <!-- icon fontawesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top d-flex align-items-center header-transparent">
    <div class="container d-flex align-items-center justify-content-between">

      <div class="logo">
        <!-- <h1><a href="index.html">Enerkom</a></h1> -->
        <!-- Uncomment below if you prefer to use an image logo -->
        <a class="d-none d-sm-block d-sm-none d-md-block d-md-none d-lg-block" style="color: blue;" href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a class="d-lg-none" href="index.html"><img src="assets/img/logos/logo_enerkom.png" alt="" class="img-fluid"></a>
      </div>

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#nosotros">NOSOTROS</a></li>
          <li><a class="nav-link scrollto" href="#empresas">EMPRESAS</a></li>
          <li><a class="nav-link scrollto" href="#services">COLABORADORES</a></li>
          <li><a style="background: none;" class="nav-link scrollto d-none d-sm-block d-sm-none d-md-block d-md-none d-lg-block" href="#contact"><img src="assets\img\logos\Enerkom_Logo-02.png" alt="" width="250px"></a></li>
          <li><a class="nav-link scrollto " href="#portfolio">FACTURACIÓN</a></li>
          <li><a class="nav-link scrollto" href="#bolsadetrabajo">BOLSAS DE TRABAJO</a></li>
          <li><a class="nav-link scrollto" href="#contacto">CONTACTO</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Carousel Section ======= -->
  <section id="hero">
    <div id="carouselExampleIndicators" class="carousel slide" data-bs-ride="carousel">
      <div id="carouselExampleDark" class="carousel carousel-dark slide" data-bs-ride="carousel">
        <div class="carousel-indicators">
          <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
          <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="1" aria-label="Slide 2"></button>
          <button type="button" data-bs-target="#carouselExampleDark" data-bs-slide-to="2" aria-label="Slide 3"></button>
        </div>
        <div class="carousel-inner">
          <div class="carousel-item active" data-bs-interval="10000">
            <img src="assets/img/slide/slide-1.jpg" class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
              <div class="row h-100 align-items-center">
                <div class="col-12 text-center">
                  <a type="button" class="btn btn-primary btn-lg buttonView" href="#nosotros" >Conoce más</a>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item" data-bs-interval="2000">
            <img src="assets/img/slide/slide-2.jpg"  class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
            </div>
          </div>
          <div class="carousel-item">
            <img src="assets/img/slide/slide-3.jpg"  class="d-block w-100" alt="...">
            <div class="carousel-caption d-none d-md-block">
            </div>
          </div>
        </div>
        <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Previous</span>
        </button>
        <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleDark" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="visually-hidden">Next</span>
        </button>
      </div>
    </div>
  </section><!-- End carousel -->
  <main id="main">
    <!-- === Nosotros Mision Section === -->
      <section id="nosotros" class="nosotros-mision">
        <div class="container">
          <div class="row" data-aos="zoom-out">
            <div class="col-lg-1"></div>
            <div class="flex-column col-lg-11 text-center text-lg-start">
              <h2 class="subtitulos">NOSOTROS</h2>
              <hr class="separador-verde">
              <hr class="separador-azul">
            </div>
          </div>
          <div class="row">
            <div class="col-lg-1">
            </div>
            <div class="col-lg-5">
              <img src="/assets/img/nosotros/Enerkom_Bandera-02.png" width="100%" alt="">
            </div>
            <div class="col-lg-5">
              <div class=" flex-column">
                <h3 class="fvision">MISIÓN</h3>
                <hr class="app-separadorve">
                <p class="tmision app-justificar">Somos un grupo empresarial familiar que satisface a sus clientes con productos y servicios innovadores y de calidad cuidando al medio ambiente y a nuestra comunidad, generando valor para nuestros colaboradores y accionistas.</p>
              </div>
            </div>
            <div class="col-lg-1"></div>
          </div>
        </div>
      </section><!-- End Nosotro Mision Section -->
      <!-- === Nosotros Visión Section === -->
      <section id="nosotros" class="nosotros-vision" style="padding: 0px;">
        <div class="container-md margin-container">
          <div class="row" data-aos="zoom-out">
            <div class="col-sm-12 col-lg-5 mt-66">
              <div class=" flex-column">
                <h3 class="fvision">Vision</h3>
                <hr class="app-separadorve">
                <p class="tvision app-justificar">Ser líderes en nuestros mercados con servicios y productos diversificados, con personal altamente capacitado y comprometido, mejores prácticas de institucionalización, tecnología de punta y solidez financiera.</p>
              </div>
            </div>
            <div class="col-sm-1"></div>
            <div class="col-sm-12 col-lg-6 vision-button">
              <img src="/assets/img/nosotros/gasolina_enerkard.jpg" class="img-responsive" alt="">
            </div>
          </div>
        </div>
      </section><!-- End Nosotro Visión Section -->
    <!-- ======= Valores Section ======= -->
    <section id="valores" class="valores">
      <div class="container">
        <div class="row">
          <div class="col-sm-1"></div>
          <div class="col-sm-11">
              <div class="flex-column">
                <h3 class="title-valores" data-aos="zoom-out">VALORES</h3>
                <hr class="separador-valores"></hr>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-1"></div>
          <div class="col-sm-5" data-aos="zoom-in">
            <h3 class="subtitulo-valor">ORIENTACIÓN AL CLIENTE</h3>
              <p class="descripcion">
                Asegurarnos de cumplir con calidad, buena actitud y respeto, lo solicitado por el cliente.
              </p>
              <br>
              <h3 class="subtitulo-valor">LEALTAD</h3>
              <p  class="descripcion">
                Respeto y compromiso entre nosotros mismos y hacia nuestra organización.
              </p>
              <br>
              <h3 class="subtitulo-valor">RESPONSABILIDAD SOCIAL</h3>
              <p  class="descripcion">
                Impactar positivamente la comunidad en la que vivimos, además de cumplir las normas ambientales y de seguridad
              </p>
          </div>
          <div class="col-sm-5" data-aos="zoom-in">
            <div class="flex-column">
              <h3 class="subtitulo-valor">CONFIANZA</h3>
              <p  class="descripcion">
                Que nuestros productos, servicios y nosotros mísmos cumplamos las expectativas de nuestros clientes, proveedores y aliados estratégicos.
              </p>
              <br>
              <h3 class="subtitulo-valor">INTEGRIDAD</h3>
              <p  class="descripcion">
                Hacer siempre lo correcto.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section><!-- End Valores Section -->

    <!-- ======= Empresas Section ======= -->
    <section id="empresas" class="empresas">
      <div class="container">
        <div class="row">
          <div class="col-sm-1"></div>
          <div class="col-sm-11">
            <div class="lex-md-row mb-4 box-shadow h-md-250 empresas">
              <div class="flex-column">
                <h2 class="titulo-empresas"> Empresas </h2>
                <hr class="separador-verde">
                <hr class="separador-azul">
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-1 hidden-md-down"></div>
            <div class="col-sm-5 hidden-md-down">
              <span class="descripcion">Las principales áreas en las que operamos:</span>
                <ul>
                  <li type="disc" class="descripcion">Estaciones de Gasolina.</li>
                  <li type="disc" class="descripcion">Distribución de Gas L.P.</li>
                  <li type="disc" class="descripcion">Transporte de Combustibles.</li>
                </ul>
            </div>
            <div class="col-sm-5">
              <span class="descripcion">&nbsp;</span>
              <ul>
                <li type="disc" class="descripcion">Distribución de Camiones.</li>
                <li type="disc" class="descripcion">Construcción e Infraestructura.</li>
            </ul>
            </div>
          <div class="col-sm-1 hidden-md-down"></div>
        </div>
        <div class="row">
          <div class="col-sm-1 hidden-md-down"></div>
          <div class="col-sm-2 hidden-md-down"><img src="/assets/img/Logos_Empresas/coreco_Logo-02.png" style="width: 100%" alt=""></div>
          <div class="col-sm-2 hidden-md-down"><img src="/assets/img/Logos_Empresas/Logo DeltaGas-02.png" style="width: 100%" alt=""></div>
          <div class="col-sm-2 hidden-md-down"><img src="/assets/img/Logos_Empresas/gulf.png" style="width: 100%" alt=""></div>
          <div class="col-sm-2 hidden-md-down"><img src="/assets/img/Logos_Empresas/lagas.png" style="width: 100%" alt=""></div>
          <div class="col-sm-2 hidden-md-down"><img src="/assets/img/Logos_Empresas/Logo plata sin fondo horizontal.png" style="width: 100%" alt=""></div>
          <div class="col-sm-1 hidden-md-down"></div>
          <br><br><br>
      </div>
      </div>
    </section><!-- End Features Section -->
    <!-- === Bolsa de trabajo Section === -->
    <section class="bolsadetrabajo" id="bolsadetrabajo" style="padding: 0px;">
      <div class="container-md margin-container">
        <div class="row">
            <!-- <div class="col-sm-1 hidden-md-down"></div> -->
            <div class="col-sm-6 col-auto hidden-md-down">
                <div class=" flex-column">
                    <h2 class="subtitulo2">BOLSA DE TRABAJO</h2>
                    <hr class="separador-verde">
                    <hr class="separador-azul">
                    <p class="descripcionbdt">Buscamos a alguien como tú, haz click en el botón y descubre los puestos que tenemos disponibles. </p>
                    <button type="button" class="btnbdt"><a href="{{route('jobs')}}" style="color: white;">VER VACANTES</a></button>
                </div>
            </div>
            <div class="col-sm-6 col-auto"><img src="/assets/img/bolsadetrabajo/15740.jpg" class="img-responsive" alt=""></div>

        </div>
      </div>
    </section>  <!-- End Bolsa de trabajo Section -->

    <!-- ======= Contact Section ======= -->
    <section id="contacto" class="contacto">
      <div class="container">
        <div class="row" data-aos="zoom-out">
          <div class="col-lg-1"></div>
          <div class="flex-column col-lg-11 text-center text-lg-start">
            <h2 class="subtitulos-contacto">CONTACTO</h2>
            <hr class="separador-verde">
            <hr class="separador-azul">
          </div>
        </div>
        <div class="row mt-5">

          <div class="col-lg-4" data-aos="fade-right">
            <div class="info">

              <div class="address">
                <i class="bi bi-geo-alt"></i>
                <h6>Dirección:</h6>
                <p>Calle 27 No. 86 x 18 y 20
                  Col. Chichén Itzá C.P. 97170
                  Mérida, Yuc, Mx.</p>
              </div>

              <div class="email">
                <i class="bi bi-envelope"></i>
                <h6>Email:</h6>
                <p>info@enerkom.com.mx
                </p>
              </div>

              <div class="phone">
                <i class="bi bi-phone"></i>
                <h6>Teléfono:</h6>
                <p>(999) 940 6000
                </p>
              </div>

            </div>

          </div>

          <div class="col-lg-8 mt-5 mt-lg-0" data-aos="fade-left">

            <form action="forms/contact.php" method="post" role="form" class="php-email-form">
              <div class="row">
                <div class="col-md-6 form-group">
                  <input type="text" name="name" class="form-control" id="name" placeholder="Nombre" required>
                </div>
                <div class="col-md-6 form-group mt-3 mt-md-0">
                  <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
                </div>
              </div>
              <div class="form-group mt-3">
                <input type="text" class="form-control" name="subject" id="subject" placeholder="Asunto" required>
              </div>
              <div class="form-group mt-3">
                <textarea class="form-control" name="message" rows="5" placeholder="Comentario" required></textarea>
              </div>
              <div class="my-3">
                <div class="loading">Loading</div>
                <div class="error-message"></div>
                <div class="sent-message">Your message has been sent. Thank you!</div>
              </div>
              <div class="text-center"><button type="submit">Enviar</button></div>
            </form>

          </div>

        </div>

      </div>
    </section><!-- End Contact Section -->

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <div class="lineArrow"></div>
        </div>
        <div class="col-sm-12">
          <span><a style="color: white;" href="#slider">
            <i class="fa fa-chevron-circle-up"></i></a>
          </span>
        </div>
      </div>
      <div style="padding-bottom: 60px;">
        <h3 class="titlefooter">ENERKOM</h3>
        <hr class="separador-verde">
        <hr class="separador-azul"><br>
        <span class="copyright">©2021. Grupo Enerkom S.A. de C.V.</span><br>
        <span class="copyright"><a href="#" style="color: white;"> Aviso de Privacidad</a></span>
      </div>

      <!-- <div class="social-links">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
      </div> -->
    </div>
  </footer><!-- End Footer -->

  <!-- <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a> -->

  <!-- Vendor JS Files -->
  <script src="assets/vendor/aos/aos.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/glightbox/js/glightbox.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/swiper/swiper-bundle.min.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>
